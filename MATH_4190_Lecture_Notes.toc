\contentsline {chapter}{\numberline {1}Introduction to mathematical modelling}{3}
\contentsline {section}{\numberline {1.1}What is a mathematical model?}{3}
\contentsline {section}{\numberline {1.2}The types of models that are emphasized in MATH 4190}{3}
\contentsline {section}{\numberline {1.3}A necessary property of a useful model}{3}
\contentsline {section}{\numberline {1.4}Example 1: Cooling cup of coffee}{4}
\contentsline {chapter}{\numberline {2}Mathematical modelling: a how to guide}{5}
\contentsline {section}{\numberline {2.1}Terminology}{5}
\contentsline {subsection}{\numberline {2.1.1}Variables and parameters}{5}
\contentsline {section}{\numberline {2.2}Derivation of a discrete time model}{6}
\contentsline {subsection}{\numberline {2.2.1}Example: Mouse model}{6}
\contentsline {section}{\numberline {2.3}Derivation of a continuous time model}{8}
\contentsline {subsection}{\numberline {2.3.1}HIV model}{8}
\contentsline {subsection}{\numberline {2.3.2}Leaky bucket}{9}
\contentsline {section}{\numberline {2.4}Making connections: discrete time and continuous time}{9}
\contentsline {section}{\numberline {2.5}Common assumptions}{10}
\contentsline {subsection}{\numberline {2.5.1}Discrete quantities modelled as continuous quantities}{10}
\contentsline {subsection}{\numberline {2.5.2}Exponential distributions}{10}
\contentsline {subsection}{\numberline {2.5.3}Same rates}{11}
\contentsline {subsection}{\numberline {2.5.4}Males and females}{11}
\contentsline {subsection}{\numberline {2.5.5}Discrete or continuous time}{11}
\contentsline {section}{\numberline {2.6}Four steps of mathematical modelling}{11}
\contentsline {subsection}{\numberline {2.6.1}Step 1: Choose a question}{12}
\contentsline {subsection}{\numberline {2.6.2}Step 2: Deriving the equations}{12}
\contentsline {subsection}{\numberline {2.6.3}Step 3: Analyze the model}{12}
\contentsline {subsection}{\numberline {2.6.4}Step 4: Interpret the results}{13}
\contentsline {subsection}{\numberline {2.6.5}Some supporting material}{13}
\contentsline {subsubsection}{Reasons to make a mathematical model}{13}
\contentsline {section}{\numberline {2.7}Non-dimensionalizing a model}{14}
\contentsline {section}{\numberline {2.8}Summary}{19}
\contentsline {chapter}{\numberline {3}Analysis of dynamical systems (Review)}{20}
\contentsline {section}{\numberline {3.1}Model solutions}{20}
\contentsline {section}{\numberline {3.2}The stability of fixed points}{20}
\contentsline {subsection}{\numberline {3.2.1}Characterizing the local stability of an equilibrium point - continuous time}{21}
\contentsline {subsection}{\numberline {3.2.2}Characterizing the local stability of an equilibrium point - discrete time}{21}
\contentsline {subsubsection}{Simple rules for characterizing the local stability of two-dimensional systems of ODEs}{21}
\contentsline {subsection}{\numberline {3.2.3}Local stability for ODEs: the Routh-Hurwitz conditions}{23}
\contentsline {subsection}{\numberline {3.2.4}Local stability for difference equations: the Jury conditions}{24}
\contentsline {section}{\numberline {3.3}Bifurcations and global stability}{26}
\contentsline {subsubsection}{Cobwebbing for recursion equations}{26}
\contentsline {subsubsection}{Phase-plane diagrams for continuous and discrete time models}{27}
\contentsline {section}{\numberline {3.4}Examples}{29}
\contentsline {subsection}{\numberline {3.4.1}Example: Continuous time analysis (linear, two coupled ODEs)}{29}
\contentsline {subsection}{\numberline {3.4.2}Discrete time analysis (non-linear, one recursion equation)}{30}
\contentsline {section}{\numberline {3.5}Numerical methods}{32}
\contentsline {section}{\numberline {3.6}Summary}{32}
\contentsline {chapter}{\numberline {4}Foundational models in biology}{33}
\contentsline {section}{\numberline {4.1}Population biology}{33}
\contentsline {subsection}{\numberline {4.1.1}Exponental/Geometric growth}{33}
\contentsline {subsection}{\numberline {4.1.2}Density-dependent growth}{33}
\contentsline {subsubsection}{Logistic growth}{33}
\contentsline {subsubsection}{Ricker model}{34}
\contentsline {subsubsection}{Beverton-Holt model}{34}
\contentsline {subsubsection}{Allee effect}{35}
\contentsline {subsection}{\numberline {4.1.3}Population models with delays}{35}
\contentsline {section}{\numberline {4.2}Age or stage structure}{35}
\contentsline {section}{\numberline {4.3}Lotka-Volterra Competition}{35}
\contentsline {section}{\numberline {4.4}Predator-prey interactions}{36}
\contentsline {section}{\numberline {4.5}Kermack-McKendrick model: Person-to-person spread}{36}
\contentsline {section}{\numberline {4.6}Ross-MacDonald: Disease-spread via vectors}{37}
\contentsline {section}{\numberline {4.7}Deriving a new model}{38}
\contentsline {chapter}{\numberline {5}Introduction to models in Physics}{39}
\contentsline {section}{\numberline {5.1}Mechanics: an undamped pendulum}{39}
\contentsline {section}{\numberline {5.2}Heat models}{39}
\contentsline {subsection}{\numberline {5.2.1}Newton's law of cooling}{39}
\contentsline {section}{\numberline {5.3}Fluid dynamics}{40}
\contentsline {chapter}{\numberline {6}Partial differential equation models}{42}
\contentsline {section}{\numberline {6.1}A population with age-structure: the McKendrick-von Foerster model}{42}
\contentsline {section}{\numberline {6.2}An SIS age of infection model}{44}
\contentsline {section}{\numberline {6.3}Growing and moving populations: reaction, diffusion and advection}{44}
\contentsline {subsection}{\numberline {6.3.1}Diffusion only}{45}
\contentsline {subsection}{\numberline {6.3.2}Reaction and diffusion: Fisher's equation}{45}
\contentsline {subsection}{\numberline {6.3.3}Can species keep pace with climate change?}{45}
\contentsline {chapter}{\numberline {7}Stochastic models}{46}
\contentsline {section}{\numberline {7.1}The Gillespie Stochastic Simulation Algorithm (SSA)}{46}
\contentsline {subsection}{\numberline {7.1.1}Numerically generating random variables}{46}
\contentsline {subsubsection}{Example: Exponential distribution}{46}
\contentsline {subsection}{\numberline {7.1.2}The Gillespie SSA}{47}
\contentsline {subsection}{\numberline {7.1.3}Characterizing the probability density of $\mu $ and $\tau $}{47}
\contentsline {subsection}{\numberline {7.1.4}Pseudo-code}{49}
\contentsline {chapter}{\numberline {8}Estimating parameters and model selection}{50}
\contentsline {section}{\numberline {8.1}Model paramterization}{50}
\contentsline {subsection}{\numberline {8.1.1}Parameter estimation}{50}
\contentsline {subsection}{\numberline {8.1.2}Transformational methods}{53}
\contentsline {subsection}{\numberline {8.1.3}Error models}{55}
\contentsline {subsection}{\numberline {8.1.4}Maximum likelihood}{56}
\contentsline {section}{\numberline {8.2}Model selection}{58}
\contentsline {subsection}{\numberline {8.2.1}Likelihood ratio test}{58}
\contentsline {subsection}{\numberline {8.2.2}Akaike Information Criterion}{59}
